package frontend

import (
	"log"
	"net/http"

	"bitbucket.org/ddtmachado/do_ma/models"
)

var BackendAddr string

func ProfileHandler(w http.ResponseWriter, r *http.Request) {
	//TODO move all auth to a middleware for all requests on frontend
	resp, err := http.Get(BackendAddr + "/sign-in")
	if err != nil {
		http.Error(w, "unable to sign in", http.StatusUnauthorized)
	}

	//TODO retrieve auth session token
	//resp.Cookie
	log.Println(resp.Status)

	//TODO replace stub profile used to test template rendering with real data
	err = profileTmpl.Execute(w, r, &models.UserProfile{FullName: "foo"})
	if err != nil {
		log.Println(err)
		http.Error(w, "error processing page", http.StatusInternalServerError)
	}
}
