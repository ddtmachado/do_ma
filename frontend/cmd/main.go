package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"bitbucket.org/ddtmachado/do_ma/frontend"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	var backendAddr string
	flag.StringVar(&backendAddr, "backend", "127.0.0.1:9090", "Backend api address. Defaults to 127.0.0.1:9090.")
	flag.Parse()

	frontend.BackendAddr = backendAddr

	r := mux.NewRouter()

	r.Handle("/", http.RedirectHandler("/sign-in", http.StatusFound))

	r.Methods("GET").
		Path("/profile").
		HandlerFunc(frontend.ProfileHandler)

	http.Handle("/", handlers.CombinedLoggingHandler(os.Stderr, r))
	log.Fatal(http.ListenAndServe(":8080", r))
}
