package frontend

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"text/template"

	"bitbucket.org/ddtmachado/do_ma/models"
)

var (
	profileTmpl = parseTemplate("profile.html")
)

func parseTemplate(filename string) *appTemplate {
	tmpl := template.Must(template.ParseFiles(filepath.Join("templates", "base.html")))

	path := filepath.Join("templates", filename)
	b, err := ioutil.ReadFile(path)
	if err != nil {
		panic(fmt.Errorf("could not read template: %v", err))
	}
	template.Must(tmpl.New("body").Parse(string(b)))

	return &appTemplate{tmpl.Lookup("base.html")}
}

// appTemplate is a user login-aware wrapper for a html/template.
type appTemplate struct {
	t *template.Template
}

// Execute writes the template using the provided data, adding login and user
// information to the base template.
func (tmpl *appTemplate) Execute(w http.ResponseWriter, r *http.Request, data interface{}) error {
	d := struct {
		Data      interface{}
		Profile   *models.UserProfile
		LoginURL  string
		LogoutURL string
	}{
		Data:      data,
		LoginURL:  "/sign-in?redirect=" + r.URL.RequestURI(),
		LogoutURL: "/sign-out?redirect=" + r.URL.RequestURI(),
	}

	//TODO
	//d.Profile = auth.ProfileFromSession(r)

	return tmpl.t.Execute(w, d)
}
