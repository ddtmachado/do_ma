package models

type UserProfile struct {
	ID        int64  `schema:"-"`
	FullName  string `schema:"name"`
	Address   string `schema:"address"`
	Telephone string `schema:"telephone"`
	Email     string `schema:"email"`
}
