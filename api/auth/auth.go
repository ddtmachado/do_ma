package auth

import (
	"net/http"

	"bitbucket.org/ddtmachado/do_ma/api/storage"
)

type AuthHandler struct {
	storage storage.AuthStoreDriver
}

//TODO setup session for auth, using a secure cookie to store the auth token
func NewAuthHandler(s storage.AuthStoreDriver) *AuthHandler {
	return &AuthHandler{
		storage: s,
	}
}

type Authenticator interface {
	Middleware(next http.Handler) http.Handler
	Authenticate(w http.ResponseWriter, req *http.Request) bool
	SetPassword(username string, password string) error
	GetPassword(username string) string
	UpdatePassword(username, oldpwd, newpwd string) error
}

func (handler *AuthHandler) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if handler.Authenticate(w, r) {
			next.ServeHTTP(w, r)
		}
	})
}

func (handler *AuthHandler) Authenticate(w http.ResponseWriter, req *http.Request) bool {
	//TODO add condition for basic or google auth
	return handler.basicAuthHandler(w, req)
}

func (handler *AuthHandler) SetPassword(username string, password string) error {
	return handler.storage.SetPassword(username, password)
}

func (handler *AuthHandler) GetPassword(username string) string {
	return handler.storage.Password(username)
}

func (handler *AuthHandler) UpdatePassword(username, oldpwd, newpwd string) error {
	return handler.storage.UpdatePassword(username, oldpwd, newpwd)
}

func (handler *AuthHandler) basicAuthHandler(w http.ResponseWriter, req *http.Request) bool {
	w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)

	username, password, authOK := req.BasicAuth()
	if authOK == false {
		http.Error(w, "Not authorized", 401)
		return false
	}

	storedPassword := handler.GetPassword(username)
	if storedPassword != password {
		http.Error(w, "Not authorized", 401)
		return false
	}
	return true
}
