package main

import (
	"database/sql"
	"flag"
	"log"

	"bitbucket.org/ddtmachado/do_ma/api/storage"

	"bitbucket.org/ddtmachado/do_ma/api"
)

func main() {
	var addr, mysqlDatasource string

	flag.StringVar(&addr, "addr", "127.0.0.1:8080", "address that the server will listen for connections. Defaults to 127.0.0.1:8080")
	flag.StringVar(&mysqlDatasource, "mysql-ds", "", "mysql address in the format user:pass@tcp(address:port)")

	flag.Parse()

	conn, err := sql.Open("mysql", mysqlDatasource)
	if err != nil {
		log.Fatal("mysql: could not get a connection: %v", err)
	}
	defer conn.Close()

	storageDriver, err := storage.NewMySQL(conn)
	if err != nil {
		log.Fatal("error setting up MySQL: %v", err)
	}
	api.NewServer(storageDriver, storageDriver).Run(addr)
}
