package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/ddtmachado/do_ma/api/auth"

	"bitbucket.org/ddtmachado/do_ma/api/storage"
	"bitbucket.org/ddtmachado/do_ma/models"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

type ApiServer struct {
	Router  *mux.Router
	Storage storage.UserStoreDriver
	Auth    auth.Authenticator
}

func NewServer(userStorage storage.UserStoreDriver, authStorage storage.AuthStoreDriver) *ApiServer {
	s := &ApiServer{
		Router:  mux.NewRouter(),
		Auth:    auth.NewAuthHandler(authStorage),
		Storage: userStorage,
	}
	s.setupRoutes()
	return s
}

func (s *ApiServer) setupRoutes() {
	directAuthHandler := func(w http.ResponseWriter, req *http.Request) {
		s.Auth.Authenticate(w, req)
	}

	s.Router.Handle("/", http.RedirectHandler("/sign-in", http.StatusFound))

	//Login and authentication
	s.Router.Path("/sign-in").
		Methods(http.MethodPost).
		HandlerFunc(directAuthHandler)
	s.Router.Path("/sign-up").
		Methods(http.MethodPost).
		HandlerFunc(s.signUp)

	//User api group
	userGroup := s.Router.PathPrefix("/user").Subrouter()
	userGroup.Use(s.Auth.Middleware)
	//profileHandler := &handlers.MethodHandler{http.MethodGet: http.HandlerFunc(s.viewProfile), http.MethodPost: http.HandlerFunc(s.updateProfile)}
	userGroup.Path("/{id}").
		Methods(http.MethodGet).
		HandlerFunc(s.viewProfile)
	userGroup.Path("/{id}").
		Methods(http.MethodPost).
		HandlerFunc(s.updateProfile)
	userGroup.Path("/{id}/sign-out").
		Methods(http.MethodPost).
		HandlerFunc(s.signOut)
	userGroup.Path("/{id}/reset-password").
		Methods(http.MethodPost).
		HandlerFunc(s.resetPassword)
}

func (s *ApiServer) Run(addr string) {
	srv := &http.Server{
		Handler:      handlers.CombinedLoggingHandler(os.Stderr, s.Router),
		Addr:         addr,
		WriteTimeout: 10 * time.Second,
		ReadTimeout:  10 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}

func (s *ApiServer) viewProfile(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	profileID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "invalid ID parameter", http.StatusBadRequest)
	}

	profile, err := s.Storage.Fetch(int64(profileID))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	err = json.NewEncoder(w).Encode(profile)

	if err != nil {
		http.Error(w, "error encoding profile data", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}

func (s *ApiServer) updateProfile(w http.ResponseWriter, r *http.Request) {
	log.Println("update profile")
	vars := mux.Vars(r)

	profileID, err := strconv.Atoi(vars["id"])
	if err != nil {
		http.Error(w, "invalid ID parameter", http.StatusBadRequest)
		return
	}

	if profileID == 0 {
		http.Error(w, "profile does not exist", http.StatusNotFound)
		return
	}

	profile := &models.UserProfile{}
	if err := parseFormData(r, profile); err != nil {
		log.Println(err)
		http.Error(w, "error decoding user profile data", http.StatusInternalServerError)
		return
	}

	//ProfileID is never retrieved from form data
	profile.ID = int64(profileID)
	if err := s.Storage.Save(profile); err != nil {
		log.Println(err)
		http.Error(w, "error storing user profile, data may be lost", http.StatusInternalServerError)
		return
	}
	log.Println("terminei aqui")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
}

func (s *ApiServer) signOut(w http.ResponseWriter, r *http.Request) {
	//TODO
}

func (s *ApiServer) signUp(w http.ResponseWriter, r *http.Request) {
	profile := &models.UserProfile{}

	if err := parseFormData(r, profile); err != nil {
		log.Println(err)
		http.Error(w, "error decoding user profile data", http.StatusInternalServerError)
		return
	}

	if err := s.Storage.Save(profile); err != nil {
		log.Println(err)
		http.Error(w, "error storing user profile, data may be lost", http.StatusInternalServerError)
		return
	}

	//When using basic auth setup the password for the user
	username, password, authOK := r.BasicAuth()
	if authOK {
		if err := s.Auth.SetPassword(username, password); err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
		}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	fmt.Fprintf(w, "{ \"id\": \"%d\"}", profile.ID)
}

func (s *ApiServer) resetPassword(w http.ResponseWriter, r *http.Request) {
	pwdForm := struct {
		Username    string `schema:"username"`
		OldPassword string `schema:"oldpwd"`
		NewPassword string `schema:"newpwd"`
	}{}

	if err := parseFormData(r, pwdForm); err != nil {
		http.Error(w, "error parsing form data", http.StatusInternalServerError)
	}

	if err := s.Auth.UpdatePassword(pwdForm.Username, pwdForm.OldPassword, pwdForm.NewPassword); err != nil {
		http.Error(w, "error updating password", http.StatusInternalServerError)
	}

	w.WriteHeader(http.StatusOK)
}

func parseFormData(r *http.Request, obj interface{}) error {
	if err := r.ParseForm(); err != nil {
		return err
	}

	decoder := schema.NewDecoder()

	return decoder.Decode(obj, r.PostForm)
}
