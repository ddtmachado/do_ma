package storage

import (
	"errors"
	"log"
	"sync"

	"bitbucket.org/ddtmachado/do_ma/models"
)

type UserStoreDriver interface {
	Save(obj *models.UserProfile) error
	Fetch(id int64) (*models.UserProfile, error)
	Close()
}

type AuthStoreDriver interface {
	Password(username string) (password string)
	SetPassword(username, password string) error
	UpdatePassword(username, oldPwd, newPwd string) error
}

type InMemoryStorage struct {
	mu     sync.Mutex
	nextID int64
	cache  map[int64]*models.UserProfile
	passwd map[int64]string
}

func NewInMemoryStorage() *InMemoryStorage {
	return &InMemoryStorage{
		cache:  make(map[int64]*models.UserProfile),
		passwd: make(map[int64]string),
		nextID: 1,
	}
}

func (s *InMemoryStorage) Fetch(id int64) (*models.UserProfile, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	log.Printf("fetching profile id %d", id)

	obj, found := s.cache[id]

	if !found {
		return nil, errors.New("profile not found")
	}

	return obj, nil
}

func (s *InMemoryStorage) Save(profile *models.UserProfile) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if profile.ID == 0 {
		profile.ID = s.nextID
	}

	log.Printf("storing object %d", profile.ID)

	s.cache[profile.ID] = profile

	s.nextID++

	return nil
}

func (s *InMemoryStorage) Password(username string) (password string) {
	s.mu.Lock()
	defer s.mu.Unlock()

	for _, p := range s.cache {
		if username == p.Email {
			log.Printf("fetching password for id %d", p.ID)

			password = s.passwd[p.ID]
			return
		}
	}

	return
}

func (s *InMemoryStorage) SetPassword(username, password string) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	for _, p := range s.cache {
		if username == p.Email {
			log.Printf("storing password for id %d", p.ID)

			s.passwd[p.ID] = password

			return nil
		}
	}

	return errors.New("profile does not exist")
}

func (s *InMemoryStorage) UpdatePassword(username, oldPwd, newPwd string) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	storedPass := s.Password(username)

	if storedPass != oldPwd {
		return errors.New("password mismatch")
	}

	return s.SetPassword(username, newPwd)
}

func (s *InMemoryStorage) Close() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.cache = nil
}
