package storage

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	"bitbucket.org/ddtmachado/do_ma/models"

	"github.com/go-sql-driver/mysql"
	_ "github.com/go-sql-driver/mysql"
)

var createTableStatements = []string{
	`CREATE DATABASE IF NOT EXISTS users DEFAULT CHARACTER SET = 'utf8' DEFAULT COLLATE 'utf8_general_ci';`,
	`USE users;`,
	`CREATE TABLE IF NOT EXISTS profiles (
		id INT UNSIGNED NOT NULL AUTO_INCREMENT,
		name VARCHAR(255) NULL,
		email VARCHAR(255) NULL,
		address VARCHAR(255) NULL,
		telephone VARCHAR(255) NULL,
		PRIMARY KEY (id)
	)`,
	`CREATE TABLE IF NOT EXISTS basic_auth (
		user_id INT UNSIGNED NOT NULL,
		secret VARCHAR(255) NOT NULL,
		PRIMARY KEY (user_id),
		FOREIGN KEY(user_id) REFERENCES profiles(id)
	)`,
}

const (
	selectUserById = `
		SELECT * FROM profiles
		WHERE id = ?`
	selectUserByEmail = `
		SELECT * FROM profiles
		WHERE email = ?`
	selectPasswordById = `
		SELECT secret FROM basic_auth
		WHERE user_id = ?`
	insertPassword = `
		INSERT INTO basic_auth (
			user_id, secret
		) VALUES (?, ?)`
	insertUser = `
		INSERT INTO profiles (
			name, email, address, telephone
		) VALUES (?, ?, ?, ?)`
	updateUser = `
		UPDATE profiles
		SET name=?, email=?, address=?, telephone=?
		WHERE id = ?`
	updatePassword = `
		UPDATE basic_auth
		SET secret=?
		WHERE user_id = ?`
)

type MySQL struct {
	dbConn             *sql.DB
	insertProfile      *sql.Stmt
	getProfileByID     *sql.Stmt
	getProfileByEmail  *sql.Stmt
	updateProfileByID  *sql.Stmt
	getPasswordByID    *sql.Stmt
	insertPassword     *sql.Stmt
	updatePasswordByID *sql.Stmt
}

func NewMySQL(conn *sql.DB) (*MySQL, error) {
	err := conn.Ping()
	if err != nil {
		conn.Close()
		log.Fatalf("unable to use connection do DB: %v", err)
	}

	//Create the database and tables if necessary
	err = ensureDatabaseAndTablesExists(conn)
	if err != nil {
		conn.Close()
		return nil, err
	}

	myDB := &MySQL{dbConn: conn}

	//Prepare the SQL statements that will be called by the api
	if myDB.getProfileByID, err = conn.Prepare(selectUserById); err != nil {
		return nil, fmt.Errorf("mysql: prepare get profile: %v", err)
	}
	if myDB.getProfileByEmail, err = conn.Prepare(selectUserByEmail); err != nil {
		return nil, fmt.Errorf("mysql: prepare get profile by email: %v", err)
	}
	if myDB.getPasswordByID, err = conn.Prepare(selectPasswordById); err != nil {
		return nil, fmt.Errorf("mysql: prepare get password: %v", err)
	}
	if myDB.insertPassword, err = conn.Prepare(insertPassword); err != nil {
		return nil, fmt.Errorf("mysql: prepare insert password: %v", err)
	}
	if myDB.insertProfile, err = conn.Prepare(insertUser); err != nil {
		return nil, fmt.Errorf("mysql: prepare insert profile: %v", err)
	}
	if myDB.updateProfileByID, err = conn.Prepare(updateUser); err != nil {
		return nil, fmt.Errorf("mysql: prepare update profile: %v", err)
	}
	if myDB.updatePasswordByID, err = conn.Prepare(updateUser); err != nil {
		return nil, fmt.Errorf("mysql: prepare update profile: %v", err)
	}

	return myDB, nil
}

func (m *MySQL) Save(obj *models.UserProfile) error {
	if obj.ID == 0 {
		r, err := execAffectingOneRow(m.insertProfile, obj.FullName, obj.Email, obj.Address, obj.Telephone)

		if err != nil {
			return err
		}

		lastInsertID, err := r.LastInsertId()

		if err != nil {
			return fmt.Errorf("mysql: could not get last insert ID: %v", err)
		}

		obj.ID = lastInsertID
		return nil
	}

	//Update profile
	_, err := execAffectingOneRow(m.updateProfileByID, obj.FullName, obj.Email, obj.Address, obj.Telephone, obj.ID)
	if err != nil {
		return err
	}

	return nil
}

func (m *MySQL) Fetch(id int64) (*models.UserProfile, error) {
	profile, err := scanProfile(m.getProfileByID.QueryRow(id))
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("mysql: could not find profile with id %d", id)
	}
	if err != nil {
		return nil, fmt.Errorf("mysql: could not get profile: %v", err)
	}
	return profile, nil
}

func (m *MySQL) getByID(email string) (*models.UserProfile, error) {
	profile, err := scanProfile(m.getProfileByEmail.QueryRow(email))
	if err == sql.ErrNoRows {
		return nil, fmt.Errorf("mysql: could not find profile with email %s", email)
	}
	if err != nil {
		return nil, fmt.Errorf("mysql: could not get profile: %v", err)
	}
	return profile, nil
}

func (m *MySQL) Close() {
	m.dbConn.Close()
}

//AuthStoreDriver interface

func (m *MySQL) Password(username string) (password string) {
	profile, err := m.getByID(username)
	if err != nil {
		log.Printf("failed to retrieve password for profile %s", username)
		return
	}
	var userID string
	row := m.getPasswordByID.QueryRow(profile.ID)
	err = row.Scan(&userID, &password)
	if err != nil {
		log.Printf("failed to read password from db for profile %s", username)
		return
	}
	return password
}

func (m *MySQL) SetPassword(username, password string) error {
	profile, err := m.getByID(username)
	if err != nil {
		log.Printf("failed to set password for profile %s", username)
		return err
	}

	_, err = execAffectingOneRow(m.insertPassword, profile.ID, password)
	return err
}

func (m *MySQL) UpdatePassword(username, oldPwd, newPwd string) error {
	profile, err := m.getByID(username)
	if err != nil {
		log.Printf("failed to set password for profile %s", username)
		return err
	}

	var userID, password string
	row := m.getPasswordByID.QueryRow(profile.ID)
	err = row.Scan(&userID, &password)
	if err != nil {
		log.Printf("failed to read password from db for profile %s", username)
		return err
	}

	if oldPwd != password {
		return errors.New("password mismatch")
	}

	_, err = execAffectingOneRow(m.updatePasswordByID, password, profile.ID)
	return err
}

//Implemented by sql.Row and sql.Rows
type rowScanner interface {
	Scan(dest ...interface{}) error
}

func scanProfile(s rowScanner) (*models.UserProfile, error) {
	var (
		id        int64
		name      sql.NullString
		email     sql.NullString
		address   sql.NullString
		telephone sql.NullString
	)
	if err := s.Scan(&id, &name, &email, &address, &telephone); err != nil {
		return nil, err
	}

	userProfile := &models.UserProfile{
		ID:        id,
		FullName:  name.String,
		Email:     email.String,
		Address:   address.String,
		Telephone: telephone.String,
	}
	return userProfile, nil
}

func ensureDatabaseAndTablesExists(conn *sql.DB) error {
	if _, err := conn.Exec("USE library"); err != nil {
		// MySQL error 1049 is "database does not exist"
		if mErr, ok := err.(*mysql.MySQLError); ok && mErr.Number == 1049 {
			return createTable(conn)
		}
	}

	if _, err := conn.Exec("DESCRIBE books"); err != nil {
		// MySQL error 1146 is "table does not exist"
		if mErr, ok := err.(*mysql.MySQLError); ok && mErr.Number == 1146 {
			return createTable(conn)
		}
		// Unknown error.
		return fmt.Errorf("mysql: could not connect to the database: %v", err)
	}
	return nil
}

func createTable(conn *sql.DB) error {
	for _, stmt := range createTableStatements {
		_, err := conn.Exec(stmt)
		if err != nil {
			return err
		}
	}
	return nil
}

func execAffectingOneRow(stmt *sql.Stmt, args ...interface{}) (sql.Result, error) {
	r, err := stmt.Exec(args...)
	if err != nil {
		return r, fmt.Errorf("mysql: could not execute statement: %v", err)
	}
	rowsAffected, err := r.RowsAffected()
	if err != nil {
		return r, fmt.Errorf("mysql: could not get rows affected: %v", err)
	} else if rowsAffected != 1 {
		return r, fmt.Errorf("mysql: expected 1 row affected, got %d", rowsAffected)
	}
	return r, nil
}
