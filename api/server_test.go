package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/ddtmachado/do_ma/models"

	"bitbucket.org/ddtmachado/do_ma/api/storage"

	"github.com/stretchr/testify/assert"
)

func TestBasicAuthSignIn(t *testing.T) {
	w := httptest.NewRecorder()
	storageDriver := storage.NewInMemoryStorage()
	req, _ := http.NewRequest(http.MethodPost, "/sign-in", bytes.NewBufferString(""))

	userProfile := &models.UserProfile{FullName: "Foo", Email: "foo@user"}
	storageDriver.Save(userProfile)
	storageDriver.SetPassword(userProfile.Email, "bar")

	req.SetBasicAuth("foo@user", "bar")
	server := NewServer(storageDriver, storageDriver)
	server.Router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestProfileUpdate(t *testing.T) {
	storageDriver := storage.NewInMemoryStorage()

	userProfile := &models.UserProfile{FullName: "Foo", Email: "foo@user"}
	storageDriver.Save(userProfile)
	storageDriver.SetPassword(userProfile.Email, "bar")

	body := bytes.NewBufferString("name=Foo%20Ok&address=R.%20Marq%20Souza%20600&telephone=12345678&email=foo@mail")
	w := httptest.NewRecorder()
	req, _ := http.NewRequest(http.MethodPost, "/user/1", body)
	req.SetBasicAuth("foo@user", "bar")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	server := NewServer(storageDriver, storageDriver)
	server.Router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	w = httptest.NewRecorder()
	req, _ = http.NewRequest(http.MethodGet, "/user/1", bytes.NewBufferString(""))
	req.SetBasicAuth("foo@mail", "bar")
	server.Router.ServeHTTP(w, req)

	var response map[string]interface{}
	json.Unmarshal([]byte(w.Body.String()), &response)
	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "foo@mail", response["Email"])
}

func TestSignUp(t *testing.T) {
	w := httptest.NewRecorder()
	storageDriver := storage.NewInMemoryStorage()

	body := bytes.NewBufferString("name=Jhon%20Doe&address=R.%20Marq%20Souza%20600&telephone=12345678&email=jhondoe@mail")
	req, _ := http.NewRequest(http.MethodPost, "/sign-up", body)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")

	req.SetBasicAuth("jhondoe@mail", "mypass")
	server := NewServer(storageDriver, storageDriver)
	server.Router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)

	var response map[string]interface{}
	json.Unmarshal([]byte(w.Body.String()), &response)

	assert.Equal(t, "1", response["id"])
}
