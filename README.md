# Simple Profile App

Basic Profile App with basic http authentication (+Google sign in [WIP]).

# Basic Project structure

```
.
├── api                         All backend related code
│   ├── auth                    Authentication handlers
│   ├── cmd                     Main binaries, could be servers, CLI, tools...
│   ├── server.go               Core backend code
│   ├── server_test.go          Core backend tests
│   └── storage                 Storage drivers (MySQL, Memory)
├── frontend                    All frontend related code
│   ├── cmd                     Main binaries, could be servers, CLI, tools...
│   ├── server.go               Core frontend code
│   ├── template.go             Template rendering code
│   └── templates               HTML templates
└── models                      Models shared between backend and frontend
```
